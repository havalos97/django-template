from django.contrib import admin
from curso.api.models import *

# Register your models here.
admin.site.register(Product)
admin.site.register(Category)